const { Router } = require("express");
const FighterService = require("../services/fighterService");
const { responseMiddleware } = require("../middlewares/response.middleware");
const {
  createFighterValid,
  updateFighterValid,
} = require("../middlewares/fighter.validation.middleware");

const router = Router();

// TODO: Implement route controllers for fighter
router.post(
  "/",
  createFighterValid,
  (req, res, next) => {
    try {
      if (!res.err) {
        const fighter = FighterService.create(req.body);
        res.data = fighter;
      }
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.get(
  "/",
  (req, res, next) => {
    try {
      const fighters = FighterService.getAll();
      res.data = fighters;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.get(
  "/:id",
  (req, res, next) => {
    try {
      const id = req.params.id;
      const fighter = FighterService.getOne({ id });

      if (!fighter) {
        res.err = "Fighter was not found";
      }

      res.data = fighter;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.put(
  "/:id",
  updateFighterValid,
  (req, res, next) => {
    try {
      if (!res.err) {
        const id = req.params.id;
        const fighter = FighterService.update(id, req.body);

        if (!fighter) {
          res.err = "Fighter was not found";
        }

        res.data = fighter;
      }
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.delete(
  "/:id",
  (req, res, next) => {
    try {
      const id = req.params.id;
      const fighter = FighterService.delete(id);

      if (!fighter) {
        res.err = "Fighter was not found";
      }

      res.data = fighter;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
