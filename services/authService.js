const UserService = require("./userService");

class AuthService {
  login(userData) {
    const user = UserService.getOne(userData);
    if (!user) {
      throw Error("User with specified credentials was not found");
    }
    return user;
  }
}

module.exports = new AuthService();
