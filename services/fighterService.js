const { FighterRepository } = require("../repositories/fighterRepository");

class FighterService {
  // TODO: Implement methods to work with fighters
  create(data) {
    const fighter = FighterRepository.create(data);
    return fighter;
  }

  getAll() {
    const fighters = FighterRepository.getAll();
    return fighters;
  }

  getOne(search) {
    const fighter = FighterRepository.getOne(search);
    return fighter;
  }

  update(id, dataToUpdate) {
    const fighter = FighterRepository.update(id, dataToUpdate);
    return fighter;
  }

  delete(id) {
    const [fighter] = FighterRepository.delete(id);
    return fighter;
  }
}

module.exports = new FighterService();
