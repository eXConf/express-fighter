const { UserRepository } = require("../repositories/userRepository");

class UserService {
  // TODO: Implement methods to work with user

  create(data) {
    const user = UserRepository.create(data);
    return user;
  }

  getOne(search) {
    const user = UserRepository.getOne(search);
    return user;
  }

  getAll() {
    const users = UserRepository.getAll();
    return users;
  }

  update(id, dataToUpdate) {
    const user = UserRepository.update(id, dataToUpdate);
    return user;
  }

  delete(id) {
    const [user] = UserRepository.delete(id);
    return user;
  }
}

module.exports = new UserService();
