const { fighter } = require("../models/fighter");
const FighterService = require("../services/fighterService");

let errors = [];

const createFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during creation

  const { body } = req;
  const { name, power, defense, health } = body;

  errors = [];

  checkExtraFields(body);
  checkIdAbsense(body);
  checkName(name);
  checkPower(power);
  checkDefense(defense);
  body.health = checkHealth(health);

  res.err = errors.join(", ");

  next();
};

const updateFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during update

  const { body } = req;
  const { name, power, defense, health } = body;

  errors = [];

  checkExtraFields(body);
  checkFieldsToUpdate(body);
  checkIdAbsense(body);

  if (name) checkName(name);
  if (power) checkPower(power);
  if (defense) checkDefense(defense);
  if (health) body.health = checkHealth(health);

  res.err = errors.join(", ");

  next();
};

function checkExtraFields(body) {
  const extraFields = Object.keys(body).filter(
    key => !Object.keys(fighter).includes(key)
  );

  if (extraFields.length > 0) {
    errors.push(`Unrequired fields: ${extraFields.join(", ")}`);
  }
}

function checkName(name) {
  if (!name) {
    errors.push("Fighter's name required");
  }

  const fighter = FighterService.getOne(
    f => f.name.toLowerCase() === name.toLowerCase()
  );

  if (fighter) {
    errors.push("Fighter with such name already exists");
  }
}

function checkPower(power) {
  if (power > 1 && power < 100) {
    return;
  } else {
    errors.push("Power should be a number between 1 and 100");
  }
}

function checkDefense(defense) {
  if (defense > 1 && defense < 10) {
    return;
  }

  errors.push("Defense should be a number between 1 and 10");
}

function checkHealth(health) {
  const defaultHealth = 100;
  if (!health) return defaultHealth;

  if (health <= 80 || health >= 120) {
    errors.push("Health should be more than 1 and less than 120");
  }
}

function checkIdAbsense(body) {
  if ("id" in body) {
    errors.push("One should not specify ID");
  }
}

function checkFieldsToUpdate(body) {
  let fields = Object.keys(body).filter(key =>
    Object.keys(fighter).includes(key)
  );

  if (fields.length >= 1) return;

  errors.push("At least one field to update required");
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
