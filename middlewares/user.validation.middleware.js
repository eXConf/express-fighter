const { user } = require("../models/user");
const UserService = require("../services/userService");

let errors = [];

const createUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during creation
  const { body } = req;
  const { firstName, lastName, email, phoneNumber, password } = body;

  errors = [];

  checkExtraFields(body);
  checkIdAbsense(body);
  checkFirstName(firstName);
  checkLastName(lastName);
  checkEmail(email);
  checkPhoneNumber(phoneNumber);
  checkPassword(password);
  res.err = errors.join(", ");

  next();
};

const updateUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during update
  const { body } = req;
  const { firstName, lastName, email, phoneNumber, password } = body;

  errors = [];

  checkExtraFields(body);
  checkFieldsToUpdate(body);
  checkIdAbsense(body);

  if (firstName) checkFirstName(firstName);
  if (lastName) checkLastName(lastName);
  if (email) checkEmail(email);
  if (phoneNumber) checkPhoneNumber(phoneNumber);
  if (password) checkPassword(password);

  res.err = errors.join(", ");

  next();
};

function checkExtraFields(body) {
  const extraFields = Object.keys(body).filter(
    key => !Object.keys(user).includes(key)
  );

  if (extraFields.length > 0) {
    errors.push(`Unrequired fields: ${extraFields.join(", ")}`);
  }
}

function checkFirstName(name) {
  if (!name) {
    errors.push("First name required");
  }
}

function checkLastName(name) {
  if (!name) {
    errors.push("Last name required");
  }
}

function checkEmail(email) {
  if (!email?.endsWith("gmail.com")) {
    errors.push("A valid Gmail address required");
  }

  const user = UserService.getOne(
    u => u.email.toLowerCase() === email.toLowerCase()
  );

  if (user) {
    errors.push("User with such email address already exists");
  }
}

function checkPhoneNumber(phoneNumber) {
  const pattern = new RegExp(/^\+380\d{9}$/);

  if (!pattern.test(phoneNumber)) {
    errors.push("Phone number should be in this format: +380xxxxxxxxx");
  }

  const user = UserService.getOne({ phoneNumber });

  if (user) {
    errors.push("User with such phone number already exists");
  }
}

function checkPassword(password) {
  if (password?.length < 3) {
    errors.push("A password containing at least 3 characters required");
  }
}

function checkIdAbsense(body) {
  if ("id" in body) {
    errors.push("One should not specify ID");
  }
}

function checkFieldsToUpdate(body) {
  let fields = Object.keys(body).filter(key => Object.keys(user).includes(key));

  if (fields.length >= 1) return;

  errors.push("At least one field to update required");
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
