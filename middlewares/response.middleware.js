const responseMiddleware = (req, res, next) => {
  // TODO: Implement middleware that returns result of the query
  let code = 200;
  let json = res.data;
  let errorJson = {
    error: true,
    message: String(res.err),
  };

  if (res.err) {
    json = errorJson;
    code = 400;
  }

  if (String(res.err).includes("not found")) {
    code = 404;
  }

  res.status(code).json(json);
  next();
};

exports.responseMiddleware = responseMiddleware;
